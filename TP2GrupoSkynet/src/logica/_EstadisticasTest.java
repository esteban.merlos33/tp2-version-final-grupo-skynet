package logica;

import static org.junit.Assert.*;

import org.junit.Test;

public class _EstadisticasTest {

	@Test(expected = NullPointerException.class)
	public void testPesoTotalGrafoNull() {
		Grafo g = null;
		Estadisticas.pesoTotalGrafo(g);
	}

	@Test
	public void testPesoTotalGrafoVacio() {
		Grafo g = new Grafo(0);
		int peso = Estadisticas.pesoTotalGrafo(g);
		assertEquals(0, peso);

	}

	@Test
	public void testPesoTotalGrafoSinAristas() {
		Grafo g = new Grafo(5);
		int peso = Estadisticas.pesoTotalGrafo(g);
		assertEquals(0, peso);
	}

	@Test
	public void testPesoTotalGrafo() {
		Grafo g = inicializar();
		assertEquals(100, Estadisticas.pesoTotalGrafo(g));
	}

	@Test
	public void testCantidadDeVerticesDeRegionVerticeAislado() {
		Grafo g = new Grafo(3);
		g.agregarArista(0, 1, 10);
		assertEquals(1, Estadisticas.cantidadDeVerticesDeRegion(g, 2));
	}

	@Test
	public void testMostrarVerticesDeRegion() {
		Grafo g = inicializar();

		assertEquals(5, Estadisticas.cantidadDeVerticesDeRegion(g, 4));

	}

	@Test
	public void testPesoRegion() {
		Grafo g = inicializar();
		assertEquals(100, Estadisticas.pesoRegion(g, 4));
	}

	@Test
	public void testPesoPromedioDeRegion() {
		Grafo g = inicializar();
		double promedio = 25.0;
		boolean equals = promedio == (10 + 20 + 30 + 40) * 1.0 / 4;
		assertTrue(equals);

	}

	private Grafo inicializar() {
		Grafo g = new Grafo(5);
		g.agregarArista(0, 1, 10);
		g.agregarArista(1, 2, 20);
		g.agregarArista(2, 3, 30);
		g.agregarArista(3, 4, 40);
		return g;
	}

}
