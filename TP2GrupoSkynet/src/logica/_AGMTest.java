package logica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class _AGMTest {

	@Test
	public void testAristaMenorParaAgregar() {
		Grafo g = new Grafo(5);

		g.agregarArista(0, 1, 10);
		g.agregarArista(0, 2, 20);
		g.agregarArista(1, 3, 30);
		g.agregarArista(1, 4, 40);

		Grafo agm = new Grafo(2);
		agm.agregarArista(0, 1, 10);

		Arista resultado = AGM.aristaMenorParaAgregar(g, agm);

		Arista esperado = new Arista(new Vertice(0), new Vertice(2), 20);
		assertEquals(resultado, esperado);
	}

	@Test
	public void testAristasAgregables() {
		Grafo g = new Grafo(5);

		g.agregarArista(0, 1, 10);
		g.agregarArista(0, 2, 20);
		g.agregarArista(1, 3, 30);
		g.agregarArista(1, 4, 40);

		Grafo agm = new Grafo(2);
		agm.agregarArista(0, 1, 10);

		ArrayList<Arista> resultado = AGM.aristasAgregables(g, agm);

		ArrayList<Arista> esperado = new ArrayList<Arista>();
		esperado.add(new Arista(new Vertice(0), new Vertice(2), 20));
		esperado.add(new Arista(new Vertice(1), new Vertice(3), 30));
		esperado.add(new Arista(new Vertice(1), new Vertice(4), 40));

		boolean verificacion = resultado.containsAll(esperado) && resultado.size() == esperado.size();
		assertTrue(verificacion);

	}

	@Test
	public void testGenerarAGMSimple() {
		Grafo g = new Grafo(4);

		g.agregarArista(0, 1, 10);
		g.agregarArista(0, 2, 30);
		g.agregarArista(0, 3, 20);

		g.agregarArista(1, 2, 20);
		g.agregarArista(1, 3, 40);

		g.agregarArista(2, 3, 30);

		Grafo esperado = new Grafo(4);
		esperado.agregarArista(0, 1, 10);
		esperado.agregarArista(0, 3, 20);
		esperado.agregarArista(1, 2, 20);

		Grafo resultado = AGM.generarAGM(g);

		assertEquals(resultado, esperado);
	}

	public void testGenerarAGM6Vertices() {
		Grafo g = new Grafo(6);

		g.agregarArista(0, 1, 20);
		g.agregarArista(0, 2, 10);
		g.agregarArista(0, 3, 30);
		g.agregarArista(0, 4, 10);

		g.agregarArista(1, 2, 10);

		g.agregarArista(2, 3, 30);
		g.agregarArista(2, 4, 20);

		g.agregarArista(3, 4, 20);
		g.agregarArista(3, 5, 30);

		g.agregarArista(4, 5, 30);

		Grafo esperado = new Grafo(6);

		esperado.agregarArista(0, 2, 10);
		esperado.agregarArista(0, 4, 10);
		esperado.agregarArista(1, 2, 10);
		esperado.agregarArista(4, 3, 20);
		esperado.agregarArista(3, 5, 30);

		Grafo resultado = AGM.generarAGM(g);
		assertEquals(resultado, esperado);
	}

	public void testGenerarAGMEjemploPDF() {
		Grafo g = new Grafo(9);
		// A=0,B=1,C=2,D=3,E=4,F=5,G=6,H=7,I=8
		g.agregarArista(0, 1, 4);
		g.agregarArista(0, 8, 8);

		g.agregarArista(1, 2, 8);
		g.agregarArista(1, 7, 12);

		g.agregarArista(2, 3, 6);
		g.agregarArista(2, 5, 4);
		g.agregarArista(2, 8, 3);

		g.agregarArista(3, 4, 9);
		g.agregarArista(3, 5, 13);

		g.agregarArista(4, 5, 10);

		g.agregarArista(5, 6, 3);

		g.agregarArista(6, 7, 1);
		g.agregarArista(6, 8, 5);

		g.agregarArista(7, 8, 6);

		Grafo agm = AGM.generarAGM(g);

		Grafo esperado = new Grafo(9);

		esperado.agregarArista(0, 1, 4);
		esperado.agregarArista(1, 2, 8);
		esperado.agregarArista(2, 8, 3);
		esperado.agregarArista(2, 5, 4);

		esperado.agregarArista(5, 6, 3);
		esperado.agregarArista(6, 7, 1);
		esperado.agregarArista(2, 3, 6);
		esperado.agregarArista(3, 4, 9);

		assertEquals(esperado, agm);

	}
}
